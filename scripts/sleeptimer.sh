#!/usr/bin/env bash
set -e

ACTION="shutdown"
CALLER_DIR="${PWD}"
DEPENDENCIES=(zenity)

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") [ARGUMENT]"
        echo "Opens a graphical user interface to set a timer for shutdown." 
        echo "ARGUMENT can be"
        echo "    --action hibernate|shutdown|suspend action to do after timeout, default: \"${ACTION}\""
        exit
    fi
done

# check dependencies
for CMD in ${DEPENDENCIES[@]}; do
    if [[ -z "$(which $CMD)" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--action" ]]; then
        shift
        ACTION="$1"
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

INPUT="$(zenity --forms --add-entry hours --add-entry minutes --add-entry seconds \
    --text "When ${ACTION}?" --title "$(basename "$0")")"
HOURS="${INPUT%|*|*}"
[[ -z "$HOURS" ]] && HOURS=0
MINUTES="${INPUT#*|}" MINUTES="${MINUTES%|*}"
[[ -z "$MINUTES" ]] && MINUTES=0
SECONDS="${INPUT#*|*|}"
[[ -z "$SECONDS" ]] && SECONDS=0
TIMEOUT="$(( "$HOURS" * 3600 + "$MINUTES" * 60 + "$SECONDS" ))"
if [[ "$TIMEOUT" -le 0 ]]; then
    echo "Error, timeout is \"${TIMEOUT}\""
    exit 1
fi
(
    for ELAPSED_TIME in $(seq 0 "$TIMEOUT"); do
        echo "# Remaining: $(( "$TIMEOUT" - "$ELAPSED_TIME" )) seconds"
        echo $(("$ELAPSED_TIME" * 100 / "$TIMEOUT" ))
        sleep 1
    done
) | zenity --progress --auto-close --title "$(basename "$0")"

if [[ "$ACTION" == "shutdown" ]]; then
    dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 \
        "org.freedesktop.login1.Manager.PowerOff" boolean:true
elif [[ "$ACTION" == "suspend" ]]; then
    dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 \
        "org.freedesktop.login1.Manager.Suspend" boolean:true
elif [[ "$ACTION" == "hibernate" ]]; then
    dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 \
        "org.freedesktop.login1.Manager.Hibernate" boolean:true
else
    echo "Action \"${ACTION}\" not supported"
fi
